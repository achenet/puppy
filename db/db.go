package db

import (
	"database/sql"
	"log"
)

var Puppies = []*Puppy{
	{
		Name:        "Jack",
		Description: "You ain't gettin' in lil man",
	},
	{
		Name:        "Coco",
		Description: "Cell block 9",
	},
	{
		Name:        "Ulysses",
		Description: "Sir Ulysses von Schwarz Wolf Barksalot of Sandalwood Lake",
	},
	{
		Name:        "Loki",
		Description: "L-Dog McPup",
	},
}

type Puppy struct {
	Name        string `json:"name",db:"name"`
	Description string `json:"description",db:"description"`
}

type DB struct {
	*sql.DB
}

func Connect() (*DB, error) {
	driver := "mysql"
	connectionString := "root:toto@tcp(172.17.0.2)/puppy"
	log.Printf("connecting to database at '%s' with driver '%s'", driver, connectionString)
	d, err := sql.Open(driver, connectionString)
	return &DB{d}, err
}

func (db *DB) ListPuppies() (out []*Puppy, err error) {
	rows, err := db.Query(`SELECT * FROM puppies;`)
	if err != nil {
		return
	}
	defer rows.Close()
	for rows.Next() {
		pup := &Puppy{}
		err = rows.Scan(&pup.Name, &pup.Description)
		out = append(out, pup)
	}
	return
}

func (db *DB) AddPuppy(p *Puppy) error {
	if _, err := db.Exec(
		`INSERT INTO puppies (name, description) VALUES (?, ?)`,
		p.Name, p.Description); err != nil {
		return err
	}
	return nil
}

//
// func (db *DB) GetPuppy(p *Puppy) (*Puppy, error) {
//
// }
//
// func (db *DB) UpdatePuppy(p *Puppy) (*Puppy, error) {
//
// }
//
// func (db *DB) DeletePuppy(p *Puppy) error {
//
// }
