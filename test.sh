#!/bin/bash
#
# shell script to run test database and API
# clear old images if needed
docker stop db
docker rm db

docker rmi pup-db

docker build -t pup-db db/
docker run -d --name db pup-db

go run main.go
