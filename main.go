package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"net/http"
	"puppy/db"
)

type api struct {
	d *db.DB
	r *gin.Engine
}

func main() {
	fmt.Println("Puppies!")

	d, err := db.Connect()
	if err != nil {
		log.Fatalf("error connecting to db: '%s'", err.Error())
	}

	r := gin.Default()
	log.Print("hi")

	a := &api{
		r: r,
		d: d,
	}

	a.r.GET("/", a.ListAllPuppies)
	a.r.GET("/:id", a.GetPuppy)
	a.r.POST("/", a.AddPuppy)
	a.r.PUT("/:id", a.UpdatePuppy)
	a.r.DELETE("/:id", a.DeletePuppy)

	a.r.Run()
}

func (a *api) ListAllPuppies(c *gin.Context) {
	c.Header("Access-Control-Allow-Origin", "*")
	puppies, err := a.d.ListPuppies()
	if err != nil {
		log.Printf("error listing puppies from db: %s", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}
	log.Printf("puppies:\n%+v\n", prettyPrint(puppies))
	c.JSON(http.StatusOK, puppies)
}

func (a *api) AddPuppy(c *gin.Context) {
	p := &db.Puppy{}
	if err := c.BindJSON(p); err != nil {
		log.Printf("error binding json: %s", err.Error())
		c.JSON(http.StatusBadRequest, gin.H{"could not bind json": err.Error()})
	}
	log.Printf("received:\n %+v\n", p)
	db.Puppies = append(db.Puppies, p)
}

func (a *api) GetPuppy(c *gin.Context) {}

func (a *api) UpdatePuppy(c *gin.Context) {}

func (a *api) DeletePuppy(c *gin.Context) {}

func prettyPrint(p []*db.Puppy) (out string) {
	for i, pup := range p {
		out += fmt.Sprintf("%d: %+v\n", i, pup)
	}
	return
}
