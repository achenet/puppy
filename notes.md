### 24 jan 2023
The app isn't rendering because there is a problem in the Table element, and more precisely in TableBody line 16: props.map isn't a function.

### 25 jan 2023
Got the app to render puppies by just doing all the work in the App component - so the issue was with props passing.
Currently having an issue where the backend sends a proper list of puppies, but there is a CORS error so the browser/frontend doesn't actually use the response.

### 26 jan 2023
From Googling/ChatGPT it seems the issue can be fixed by setting the `Access-Control-Allow-Origin`, and setting this in gin does indeed fix it. Now why does the table disappear when it gets the new payload from the server?

Also, `each child in a list should have a unique "key" prop`, and `ReactDOM.render` is deprecated, use `createRoot` instead.

Table disappears because it's returning `data == undefined` when it calls the API and gets the data from json...

### 28 jan 2023
Looks like a JSON parsing error. Maybe add a "catch" callback after the `response.json()` callback?
Yeah, turns out if you define the callback with curly braces, you need to add a return,
whereas if you define it without curly braces in one line, you can omit the return statement.
I used curly braces by didn't explicitly say `return response.json()`, hence the error.
It works now, we can officially fetch stuff from the API :DDD

Have a pair of warnings to clean up-
use keys for lists,
ands use `createRoot` instead of `ReactDOM.render` to create the root node.

Now time to add a POST to create a puppy.
Can't be fucked to set up the db properly yet, we'll use a package level variable in the `db` package to hold state for now.

### 29 jan 2023
Somewhat pressed for time today with AcroYoga stage. Note that `mode: 'no-cors'` is a valid option for `fetch()`.
I guess that's another way to resolve the CORS error I was getting earlier.

### 30 jan 2023
Added basic posts, now how do I create a form so the user can specify data, and pass that data as the JSON body of the POST request?

### 31 jan 2023
Form created following Tania Rascia's react tutorial, does not work. as in, clicking the submit button does nothing, error with prop passing?
The `addPuppy` function isn't even getting called. Fixed it by renaming `submitForm` to `handleSubmit`. Not sure exactly why that fixed it, should probably look into that at some point.

Currently, the backend will try to list all puppies after the post, however the front doesn't update, so change it to have the front do a GET after sending the post.

Trying to do that, now the POST returns a 400... `Uncaught (in promise) SyntaxError: JSON.parse: unexpected end of data at line 1 column 1 of the JSON data`
Seems the cause is on the server - `json: cannot unmarshal string into Go struct field Puppy.pup of type int`
err... okay... Maybe the call to `ListAllPuppies` at the end of the `AddPuppy` handler was obscuring this by overwriting thge 400 with a 400.

### 1 feb 2023
Really ghetto workaround- delete the `Pup int` field, just have two strings... :P
It works ^_^  ❤️

I guess now time to properly set up the backend, connect to database, dockerize and make a small shell script to automatically launch a test db + api,
then add in the `delete` and `update` functions and we gucci.

Okay... created basic `test.sh`, have db running (in theory) in a Docker container, but getting a nil pointer derefence error when I ask the API to do a query in the db...

Yeah, it's not properly connecting to the database ... :/

At some point I'll also want to set all this up so it actually runs on a proper server somewhere. I have an OVH VPS I can use. According to ChatGPT, one way to do this is just `npm build` to get a final, static file of the frontend, stick it on nginx and have it send that to the client. Of course, then I'll have to figure out the routing with the backend. I recall Titan had some sort of 'nginx reverse proxy' system going, so maybe I could try something similar.
