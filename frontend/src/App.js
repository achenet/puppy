import React from 'react'

class Form extends React.Component {
  initialState = {
    name: '',
    description: '',
    pup: null,
  }

  handleChange = (event) => {
    const { name, value } = event.target

    this.setState({
      [name]: value,
    })
  }

  handleSubmit = () => {
    this.props.handleSubmit(this.state)
    this.setState(this.initialState)
  }

  render() {
    return (
    <form>
      <label htmlFor="name">Name</label>
      <input
        type="text"
        name="name"
        id="name"
        onChange={this.handleChange} />
      <label htmlFor="description">Description</label>
      <input
        type="text"
        name="description"
        id="description"
        onChange={this.handleChange} />
      <input type="button" value="Submit" onClick={this.handleSubmit} />
    </form>
    )
  }
}

class App extends React.Component {
  state = {
    puppies: [ {
      name: 'Loki',
      description: 'The one and only',
    },
    { name: 'Bobby',
      description: 'Oh boy!'
    },
    ],
  }

  getPuppies = () => {
    const url = "http://localhost:8080/"
    fetch(url)
      .then((response) => {
        return response.json()
      })
      .then((data) => {
        console.log("setting state")
        console.log("data:", data)
        this.setState({
          puppies: data,
        })
      })
      .catch( (err) => {
        console.log("error parsing json")
        console.error(err)
      })
  }

  addPuppy = (pup) => {
    console.log("postin'")
    const url = "http://localhost:8080/"
    const opts = {
      method: 'POST',
      mode:  'no-cors',
      headers: new Headers({ 'content-type' : 'application/json' }),
      body: JSON.stringify(pup)
    }
    fetch(url, opts)
      .then( () => this.getPuppies() )
  }

  render() {
    const { puppies } = this.state
    const rows = this.state.puppies.map(pup => <div> <h3 key={pup.name}>{pup.name}</h3> <p>{pup.description}</p> </div>)
    return (
      <div className="container">
        <h1>Puppies!</h1>
          <div>{rows}</div>
        <button onClick={() =>  this.getPuppies() }>Get Puppies!</button>
        <Form handleSubmit={this.addPuppy}/>
        <div className="container">
        </div>
      </div>
    )
  }
}

export default App
